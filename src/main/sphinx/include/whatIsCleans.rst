##################
What is |project|?
##################

|project| belongs to the new collection of Asset Control Services such as OPS360 Dashboard, Data Browsing and the Core UI Service.
They are developed based on the MicroService architecture.

|project| is a system controlling
#################
How does it work?
#################

|project|

############################
AC MicroService Architecture
############################

A MicroServices architecture is a collection of independently deploy-able services.
Every service has its own function and will be deployed in its own Docker container.
Every container has its own isolated workload environment making it independently deploy-able and scalable.
Each individual AC Service runs in its own isolated environment, separate from the others within the architecture.
Some of the new AC Services are Web applications having their own Web User Interfaces (UI). They are developed to
provide the the ability to configure a single unified Web UI (AC Web UI).
The AC Core UI Service is the core component of the AC Services representing the common properties of this UI.
