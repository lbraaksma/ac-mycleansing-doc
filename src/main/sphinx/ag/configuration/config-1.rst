The |project| can be configured with the following steps:

* **Step 1.** Add Java Keystore file
* **Step 2.** Add properties to the docker.conf file.
* **Step 3.** Provide Service permissions to users
* **Step 4.** Make the Service available to a proxy service


**Step 1.**

|project| requires a Java keystore with a private-public key pair aliased 'ac-authentication' by default.
Keystore must be located in folder ``/opt/docker/conf/external`` of the |project| container.
It can also be provided via external volume ``/myhost/docker-volumes/auth/external`` like in our example.
The other AC Webservices need to add its public key to this keystore so the |project| and these AC Webservices can communicate.
You can read more on to add keys on the following website: ``https://docs.oracle.com/javase/tutorial/security/toolsign/step3.html`` for example.


**Step 2.**

The following properties can be added to the docker.conf file.
In our docker-compose example we have put this file on volume ``/myhost/docker-volumes/auth/load``.
And we added ``command:  -Dconfig.file=/opt/docker/conf/external/docker.conf`` to the docker-compose file so the the container can find this file.

Some of these |project| properties can also be provided as environment variables

.. acprop:: ac.authentication.keystore.file

    Location and name of the keystore file. Default value: conf/keystore.jks

.. acprop:: ac.authentication.keystore.password

    Password for the keystore file.

.. acprop:: ac.authentication.key.alias

    Alias for the private/public key pair located in keystore file that |project| should use.

.. acprop:: ac.authentication.key.password

    Password for key pair.

.. acprop:: ac.authentication.cipher.algorithm

    Algorithm that |project| should use to sign user rights.

.. acprop:: ac.authentication.rights.cookie.name

    Name of the cookie containing user rights, that client application will receive after authentication.

.. acprop:: ac.authentication.password.expiration.enabled

    Controls if user passwords should be checked for expiration on each authentication request.

.. acprop:: ac.authentication.password.expiration.days

    Number of days until user password expires and must be changed.

.. acprop:: ac.authentication.enabled

    Controls whether |project| checks user rights for requests that require authentication

.. acprop:: ac.authentication.rights.expiration.enabled

    Controls whether |project| checks rights for expiration.

.. warning::

    If several properties are defined for *ac.authentication.rights.expiration* only one of them is used.

.. acprop:: ac.authentication.rights.expiration.seconds

    Amount of seconds until rights are deemed as expired.

.. acprop:: ac.authentication.rights.expiration.minutes

    Amount of minutes until rights are deemed as expired.

.. acprop:: ac.authentication.rights.expiration.hours

    Amount of hours until rights are deemed as expired.

.. acprop:: ac.authentication.rights.expiration.days

    Amount of days until rights are deemed as expired.


**Step 3.**

Nothing needs to be done in this step. These permissions are automatically added by start of the |project|.

Users need permissions for a specific :ref:`ag-service` in order to use different parts of |project| API:

.. acprop:: AUTHENTICATION.SERVICES

    For access to API dealing with :ref:`ag-service` objects

.. acprop:: AUTHENTICATION.DATAVIEWS

    For access to API dealing with :ref:`ag-dataview` objects

.. acprop:: AUTHENTICATION.ROLES

    For access to API dealing with :ref:`ag-role` objects

.. acprop:: AUTHENTICATION.USERS

    For access to API dealing with :ref:`ag-user` objects

.. acprop:: AUTHENTICATION.ALL

    For full access to API


**Step 4.**

Make the Service available to a proxy service by specifying:

.. code-block:: console

   proxy all /issue-service/ to issue-service:9000/issue-service/
   proxy all /cleansing/ to data-cleansing-service:80/

As an example, when a NGINX server is used as a proxy server, the configuration file needs to be updated.
The configuration file of a NGINX server can be found in the folder ``/etc/nginx/available-sites``.
The following lines need to be added to this file:

.. code-block:: console

   location /issue-service/ {
      proxy_pass http://issue-service:9000/issue-service/;
      access_log  /var/log/nginx/access.log  upstream_logging;
   }

   location /cleansing/ {
      proxy_pass http://data-cleansing-ui:80/;
      access_log  /var/log/nginx/access.log  upstream_logging;
   }